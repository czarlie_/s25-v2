console.log('Hello World 👋 🌏')

/*
  JSON
    - Stands for JavaScript Object Notation
    - Is also used in other programming languages
    - are wrapped in curly braces
    - properties/Keys are wrapped in double quotations
    
    Syntax:
    {
      "propertyA:" "valueA"
      "propertyB:" "valueB"
      "propertyC:" "valueC"
    }
*/

let sample1 = `  {
    "name": "Cardo Dalisay",
    "age": 20,
    "address": {
      "city": "Quezon City",
      "country": "Philippines"
    }
  }
`
console.log(sample1)

// Mini Activity #1
// Create a variable that will hold a "JSON" and create a person objet

const luffy = `{
  "name": "Monkey D. Luffy",
  "age": 17,
  "address": {
    "city": "Windmill Village",
    "country": "Dawn Island"
  }
}
`

const zoro = `{
  'name': 'zoro',
  'age': 19,
  'address': {
    'city': 'Shimotsuki Village',
    'country': 'Kuraigana Island'
  }
}
`

console.log(luffy)
console.log(typeof luffy)

console.log(zoro)
console.log(typeof zoro)

// Are we able to turn a JSON into a JavaScript Object?
// JSON.parse() - This will return the JSON as an Object

console.log(JSON.parse(sample1))
console.log(JSON.parse(luffy))

// JSON Array
//  - Array of JSON

let sampleArray = `
  [
    {
      "email": "sinayangmoako@gmail.com",
      "password": "february14",
      "isAdmin": false
    },
    {
      "email": "dalisay@cardo.com",
      "password": "thecountryman",
      "isAdmin": false
    },
    {
      "email": "jsonv@gmail.com",
      "password": "fridaythe13",
      "isAdmin": true
    }
  ]
`
console.log(sampleArray)
console.log(typeof sampleArray)

// Can we use Array Methods on a JSON Array?
// No. Because a JSON is a string

// So what can we do to be able to add more items/objects into our sampleArray?

// PARSE the JSON array to a JavaScript array and save it in a variable

let parsedSampleArray = JSON.parse(sampleArray)
console.log(parsedSampleArray)
console.log(typeof parsedSampleArray)

// can we now delete the last item in the JSON array?
console.log(parsedSampleArray.pop())
console.log(parsedSampleArray)

// if for example we need to send this data back to our client/front-end, it should be in JSON format
// JSON.parse() does not mutate or update the original JSON
// We can actually turn a JavaScript into a JSON
// JSON.stringify() - this will stringify JavaScript objects as JSON

sampleArray = JSON.stringify(parsedSampleArray)
console.log(sampleArray)

// Database (JSON) => Server/API (JSON to JavaScript Object to process) => sent as JSON => frontend/client

/*
  Mini Activity #2
  - Given the JSON array, process it and convert to a JavaScript Object so we can manipulate the array; then
  - Delete the last item in the array and add a new item in the array; afterwards
  - Stringify the array back in JSON; and
  - Update jsonArray with the stringified array

*/

let jsonArray = `
  [
    'pizza',
    'hamburger',
    'spaghetti',
    'shanghai',
    "hotdog stick on a pineapple",
    "pancit bihon"
  ]
`
let jsonArrayDoubleQuotes = `
  [
    "pizza",
    "hamburger",
    "spaghetti",
    "shanghai",
    "hotdog stick on a pineapple",
    "pancit bihon"
  ]
`
console.log(jsonArrayDoubleQuotes)
let parsedJsonArray = JSON.parse(jsonArrayDoubleQuotes)
let poppedParsedJsonArray = parsedJsonArray.pop()
parsedJsonArray.push('carbonara')
let resultOfParsedArray = JSON.stringify(parsedJsonArray)
console.log(resultOfParsedArray)

/*
console.log(jsonArrayDoubleQuotes)
let parsedJsonArray = JSON.parse(jsonArrayDoubleQuotes)

parsedJsonArray.pop()
parsedJsonArray.push('Ice cream!')

jsonArrayDoubleQuotes = JSON.stringify(parsedJsonArray)
console.log(jsonArrayDoubleQuotes)
*/

// Gather User Details
let firstName = prompt('What is your first name?')
let lastName = prompt(`What is your last name?`)
let age = prompt('What is your age?')
let address = {
  city: prompt('Which city do you live in?'),
  country: prompt('which country does your city address belong to?'),
}

let otherData = JSON.stringify({
  firstName: firstName,
  lastName: lastName,
  age: age,
  address: address,
})
console.log(otherData)
